import Vue from 'vue'
import App from './App.vue'

import './styles/reset.styl'
import './styles/main.styl'

Vue.config.productionTip = false
import { SplitDirective } from './directives/SplitDirective'

Vue.directive('split', SplitDirective)

new Vue({
  render: h => h(App),
}).$mount('#app')
